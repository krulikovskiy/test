var globalDeliveryConfig = {
// === ОБЯЗАТЕЛЬНО ИЗМЕНИТЬ НА СВОИ ДАННЫЕ ===
    "typeRestaurants": "local",         // local- отправлять все заказы всегда в ресторан, указанный в objectId. Если удалить данное поле, то будут использоваться зоны доставки
    "corpId": "43381",                  // 5-значный номер компании
    "objectId": "433810001",            // 9-значный номер объекта по умолчанию, если в GPX ресторан не был найден
    "sid": "BqTml8IDIE6D8TmhBfZCb9Kk7jv3WECuiST4ZxM9YhAyLCdKOfLAD3yCftpBNPqE",                         // Берётся из личного кабинета по адресу http://dlv.ucs.ru/admin
    "keyMapApiGoogle": "AIzaSyCwT3Xsyip1trERVonIId-uwHfaSLjkRyg",             // Необходимо получить ключ https://developers.google.com/maps/documentation/geocoding/get-api-key?hl=ru
// ===========================================
    "urlService": "https://dlv.ucs.ru", // Основной адрес серверной части
    "showDetailError": 1,               // Показывать подробное описание ошибок  1/0
    "local": "ru",                      // Локализация ru/en
    "currency": "руб.",                    // Валюта
    "addTimes": 1,                      // В часах (только целые значения), при заказе "на ближайшее время" добавляем этот параметр к текущему времени
    "periodNoDelivery": 60,             // В минутах, период в течении которого доставка еще работает, но заявки уже не принимаются (перед завершением работы),
    "timeOutGetCoord": 10000,           // В миллисекундах, таймаут для запроса координат курьера
    "maskPhone": "9(999) 999-99-99",     // Допустимые символы, помимо 9: "(", ")", "-", " " (скобки, короткий дефис, пробел)
};