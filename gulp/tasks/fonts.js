var gulp = require('gulp'),
	$ = require('gulp-load-plugins')(); // автоматическая загрузка плагинов gulp

// копируем шрифты
gulp.task('fonts', function () {
	return gulp.src(['../media/fonts/**/*'])
		.on('error', function () {
			this.emit('end');
		})
		.pipe(gulp.dest('../assets/fonts/')); // Путь до компилированных скриптов
});