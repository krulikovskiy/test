var gulp = require('gulp'),
	$ = require('gulp-load-plugins')();
	var less = require('gulp-less');


gulp.task('styles', function () {
	return gulp.src([
			'../media/css/reset.css',
			'../media/css/bootstrap.css',
			'../media/css/jquery-ui.css',
			'../media/css/helper.less',
			'../media/css/font-awesome.css',
			'../media/css/style.less',
			'../media/css/response.less'
		])
		.pipe($.concat('style.css')) // Объединяем в один файл
		.pipe(less()) // Запускаем less
		.on('error', function () {
			this.emit('end');
		})
		.pipe($.csso()) // Минификация
		.pipe(gulp.dest('../assets/css/')); // Путь до компилированных стилей
});
