// Подключаем gulp
var gulp = require('gulp');

// gulp build (вторым параметром в массив передаются задачи которые запускаются перед выполнением данной задачи)
gulp.task('build', ['scripts','vendor','images','styles','fonts']);

// Команда по умолчанию, то что будет происходить, если вызвать просто gulp
gulp.task('default', ['build']);

