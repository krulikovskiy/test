var gulp = require('gulp'),
	$ = require('gulp-load-plugins')(); // автоматическая загрузка плагинов gulp

// собираем сторонние библиотеки
gulp.task('vendor', function () {
	return gulp.src([
			'../media/js/vendor/*.js',
			'../media/js/angular/lodash.min.js',
			'../media/js/angular/angular.js',
			'../media/js/angular/ngStorage.js',
			'../media/js/angular/angular-route.js',
			//'../media/js/angular/angular-ui-router.min.js',
			'../media/js/angular/angular-touch.js',
			'../media/js/angular/angular-ui-autocomplete.js',
			'../media/js/angular/ui-bootstrap-tpls-0.12.1.min.js',
			'../media/js/angular/ngMask.min.js',
			'../media/js/angular/angular-simple-logger.js',
			'../media/js/angular/angular-google-maps.min.js',
		])
		.pipe($.concat('vendors.js')) // Объединяем в один файл
		.pipe($.uglify()) // Минификация
		.on('error', function() {
			this.emit('end');
		})
		.pipe(gulp.dest('../assets/js/')); // Путь до компилированных скриптов

});

gulp.task('scripts', function () {
	return gulp.src([
			'../media/js/App.js',
			'../media/js/service/configService.js',
			'../media/js/service/localizationService.js',
			'../media/js/service/requestService.js',
			'../media/js/controllers/*.js',
		])
		.pipe($.concat('script.js')) // Объединяем в один файл
		.pipe($.uglify()) // Минификация todo
		.on('error', function () {
			this.emit('end');
		})
		.pipe(gulp.dest('../assets/js/')); // Путь до компилированных скриптов
});
