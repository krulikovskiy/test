var gulp = require('gulp'),
	$ = require('gulp-load-plugins')(); // автоматическая загрузка плагинов gulp

// копируем шрифты
gulp.task('images', function () {
	return gulp.src(['../media/img/**/*'])
		.on('error', function () {
			this.emit('end');
		})
		.pipe(gulp.dest('../assets/img/')); // Путь до компилированных скриптов
});