var gulp = require('gulp'),
	$ = require('gulp-load-plugins')();

gulp.task('watch', ['scripts','vendor','styles','fonts'], function() {
	gulp.watch(['../media/js/service/*.js', '../media/js/controllers/*.js', '../media/js/App.js'], ['scripts']);
	gulp.watch(['../media/js/angular/*.js', '../media/js/angular/vendor/*.js'], ['vendor']);
	gulp.watch('../media/css/*', ['styles']);
	gulp.watch('../media/fonts/*', ['fonts']);
});
