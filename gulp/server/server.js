let express = require('express')
let fs = require('fs')
let bodyParser = require('body-parser')
let dateFormat = require('dateformat')

let server = express()
server.use(express.json())

let urlencodedParser = bodyParser.urlencoded({ extended: false })
let jsonParser = bodyParser.json()

server.use('/', express.static('../'))


server.listen(8080, () => { console.log('listening at port 8080') })