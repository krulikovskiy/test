app.controller('payResultCtrl', ['$scope', '$rootScope', '$location', 'requestServ',
	function($scope, $rootScope, $location, requestServ) {

	$scope.switchPage = 'load';
	$scope.dataArray = $location.search();
	$scope.switchPage = $scope.dataArray.status;

	$scope.getPayStatus = function() {

		var options = {
			method: 'GET',
			param: {
				"visitId": $scope.dataArray.visit_id,
				"objectId": $scope.dataArray.object_id,
				//"billNumber": $scope.dataArray.visit_id + '_' + $scope.dataArray.object_id
			}
		};

		$scope.switchPage = 'load';

		requestServ.serverCall("/api/v1/assist/orderstate", options, function(data) {
			$scope.switchPage = 'load';

			if(data.error) {
				$scope.switchPage = 'hide';

				var options = {
					message: $rootScope.getLocale('Error checking payment status'),
					fullMessage: data.value,
					type: 'warning',
					callbackOk: function() {
						$scope.getPayStatus();
						$scope.switchPage = 'load';
						$rootScope.CloseError();
					},
					callbackCancel: function() {
						$rootScope.CloseError();
						$scope.switchPage = 'error';
					}
				};
				$rootScope.ShowErrorMessage(options);
			} else {
				$scope.switchPage = 'success';

				//console.log(data);

				$scope.dataArray.billnumber = data.value.data.bill_number;
				$scope.dataArray.textError = data.value.data.error;

			}
		});
	};

	//if(dataArray.type == 'ASSIST') {
	//
	//	var options = {
	//		method: 'POST',
	//		data: {
	//			"object_id": dataArray.object_id,
	//			"visit_id": dataArray.visit_id,
	//			"visit_guid": dataArray.visit_guid,
	//			"payments": [
	//				{
	//					"currency_id": dataArray.currency_id,
	//					"extpay_id": dataArray.type,
	//					"amount": dataArray.amount
	//				}
	//			]
	//		}
	//	};
	//
	//	console.log(options);
	//
	//	$scope.setStatus = function() {
	//		requestServ.serverCall("payment", options, function(data) {
	//			if(data.error) {
	//				console.log(data);
	//				console.log(data.error);
	//				$scope.switchPage = 'hide';
	//
	//				var options = {
	//					message: $rootScope.getLocale('Error checking payment status'),
	//					type: 'warning',
	//					callbackOk: function() {
	//						$scope.setStatus();
	//						$scope.switchPage = 'load';
	//						$rootScope.CloseError();
	//					},
	//					callbackCancel: function() {
	//						$rootScope.CloseError();
	//						$scope.switchPage = 'error';
	//					}
	//				};
	//				$rootScope.ShowErrorMessage(options);
	//			} else {
	//				$scope.switchPage = 'success';
	//			}
	//		});
	//	};
	//
	//	$scope.setStatus();
	//
	//} else {
	//	$scope.switchPage = 'error';
	//}
}]);


