app.controller('catalogCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

	$scope.$watch('currentPage + numPerPage', function () {
		$scope.updatePagination({
			currentPage: $scope.currentPage,
			numPerPage: $scope.numPerPage
		});
	});

	$rootScope.isCart = false;
	$scope.initProducts();
}]);

app.directive('selectCategory', function () {
	return {
		restrict: 'A',
		link: function(scope, element, attr) {
			element.bind("click", function() {
				element.addClass('current')
					.parents('.js-menu-category')
					.find('.js-child')
					.not(element)
					.removeClass('current');
			});
		}
	};
});

app.directive('showCategory', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attr) {
			element.bind("click", function() {
				element.toggleClass('active')
					.parent('.item')
					.toggleClass('show-category');
			});
		}
	};
});

