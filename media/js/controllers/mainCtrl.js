app.controller('mainCtrl', ['$scope', '$location', '$http','$rootScope', '$localStorage', 'localisation', 'configServ', '$sce',
	function($scope, $location, $http, $rootScope, $localStorage, localisation, config, paginationConfig, $sce) {

		$scope.endLoadClass = 'h-load-end'; //отображаем содержимое страницы
		/* -- Настройки -- */
		$rootScope.currentUrl = $location.$$path;
		$rootScope.Currency = config.currency;
		$rootScope.currentLocal = config.local;
		$rootScope.showDetailError =  config.showDetailError;
		$rootScope.sce = $sce;


		$scope.keyApi = config.keyMapApiGoogle;
		$scope.defaultCartInfo = {
			ResultSumm: 0,
			ResultCount: 0,
			cartList: []
		};
		$scope.defaultUserInfo = {
			addressDelivery: {},
			restaurantInfo: {},
			typeDelivery: ''
		};

		$rootScope.toggleDetailError = function() {
			$rootScope.hideDetailError = !$rootScope.hideDetailError;
		};

		$rootScope.showHomeIcon = function() {
			// показать иконку перехода только на определенных страницах
			var pageName = ['/catalog'];
			var showSwitch = false;

			if(pageName.indexOf($location.$$path) >= 0) {
				showSwitch = true;
			}
			return showSwitch;
		};

		$scope.saveUser = function() {
			$localStorage.UserInfo = $rootScope.UserInfo;
		};


		/* Действия с корзиной */
		$scope.saveCart = function() {
			$localStorage.CartInfo = $rootScope.CartInfo;
		};

		$scope.clearCart = function() {
			$rootScope.CartInfo = {
				ResultSumm: 0,
				ResultCount: 0,
				cartList: []
			};
			$localStorage.CartInfo = $rootScope.CartInfo;
		};

		$scope.deleteLocalProducts = function() {
			$rootScope.RestaurantMenu = '';
			$rootScope.menutree = '';
			$rootScope.UserInfo.restaurantInfo = [];
		};


		$rootScope.CartInfo = $localStorage.CartInfo || $scope.defaultCartInfo;
		$rootScope.UserInfo = $localStorage.UserInfo || $scope.defaultUserInfo;
		$rootScope.menutree = [];
		$rootScope.RestaurantMenu = {};

	}]);