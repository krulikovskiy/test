app.controller('cartCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
	$scope.cartProduct = [];
	$scope.countProductCart = [];
	$scope.countSubProductCart = [];
	$scope.modisSelectCart = [];
	$rootScope.isCart = true;
	$scope.errorCart = false;

	$rootScope.loadCart = function() {
		// формируем сисок для отображения в корзине

		$rootScope.CartInfo.cartList.forEach(function (value, i) {
			$scope.priceSubProduct[value.order] = [];
			$scope.countSubProduct[value.order] = [];
			var item = [];
			var info = $scope.GetProduct(value.product, value.category);

			item.info = info;
			item.order = value.order;

			$scope.cartProduct.push(item);

			$scope.countProduct[value.order] = value.count;
			$scope.modisSelect[value.order] = [];

			value.modis.forEach(function (valueModis, iModis) {
				$scope.countSubProduct[value.order][valueModis.id] = valueModis.value;
			});

			value.modisSelect.forEach(function (valueModis, iModis) {
				$scope.modisSelect[value.order][valueModis.id] = valueModis.value;
			});

			$scope.GetResultPrice(value.order, value.product, true);

			$scope.helperInitCart();
		});

 	};

	$scope.deleteProduct = function(idProduct, index) {
		// удаляем продукт из корзины
		angular.element('#product-' + index)
			.fadeOut(600);
		var indexProduct;
		var cartList = $rootScope.CartInfo.cartList;
		cartList.forEach(function(value, i) {
			if(value.order == index) {
				indexProduct = i;
			}
		});

		cartList.splice(indexProduct, 1);
		if($rootScope.CartInfo.cartList.length == 0) {
			$scope.cartProduct = [];
			$scope.clearCart();
		}

		$scope.updateCartInfo();
	};

	$scope.helperInitCart = function() {

		$scope.cartProduct.forEach(function(value, i){

			// ставим ограничение на максимальные значения для групп модификаторов
			if(value.info.modis && value.info.modis.inputModis) {
				$scope.disabledIncSubProduct[value.order] = {};

				value.info.modis.inputModis.forEach(function(valueMini, itemMini) {
					$scope.checkMaxValueGroup({
						idProduct: value.order,
						idParent: valueMini.id,
						maxGroupCount: valueMini.upLimit
					});
				});
			}
		});
	};

	$scope.checkMinValueCart = function() {
		var error = false;

		$rootScope.CartInfo.cartList.forEach(function(value, i){

			$scope.checkMinValueGroup({
				element: angular.element('#product-' + value.order),
				type: 'cart',
				attrs: {
					idProduct: value.order
				}
			});

			// если нет ошибок
			angular.forEach($scope.errorMinCount[value.order], function(value, key) {
				if(value) {
					error = true;
				}
			});

		});

		$scope.errorCart = error;

		if(!error) {
			window.location.href = '#/order';
		}
	};

	$scope.initProducts();

}]);

