app.factory("localisation",["$rootScope",'$localStorage','configServ', '$http',
    function($rootScope, $localStorage, config, $http) {
    return{
        loadLocale: function() {
            $http.get('config-app/local/locale-' + config.local + '.json')
                .success(function (data) {
                    //$rootScope.locale = JSON.parse('{' + data + '}');
                    $rootScope.locale = data;
                }).error(function() {
                    $rootScope.ShowErrorMessage({
                        message: 'Error loading file localization',
                        type: 'error'
                    });
                });



            //$rootScope.locale = $localStorage.locale;

        },
        localize: function(str) {
            var resultDefault = '"[' + str + ']" not translate';
            var result = resultDefault;

            try {
                if(!($rootScope.locale == undefined || $rootScope.locale[str] == undefined)) {
                    result = $rootScope.locale[str];
                }

                return result;

            } catch(e) {
                return resultDefault;
            }
        }
    }
}]);