app.factory("requestServ",["$http","configServ",'$rootScope','$location',function($http,config,$rootScope,$location){
    return{
        serverCall:function(url, options, callback) {
            var params = options.param || [];
            var data = options.data || [];
            var method = options.method || 'GET';
            var response = {
                error: false,
                value: {}
            };
            
            console.log(data);
		
		console.log('####!!!');
		console.log(params);
		console.log(config.urlService + url);
		console.log('####!!!');

            $http({
                url: config.urlService + url,
                method: method,
                headers: {
                    'sid': config.sid,
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                params: params,
                data: data
            }).success(function( result ) {

                if(result.status == "Err") {
                    response.error = true;
                    if(result.error) {
                        response.value = result.error[0];
                    }
                } else {
		    console.log('####');
		    console.log(result);
		    console.log(method);
		    //console.log(headers);
		    
		    //console.log(data);
		    console.log(url);
		    console.log('####');
                    response.value = result;
                }
                console.log('response:');
                console.log(response);
		console.log('end_response');
                
		callback(response);


            }).error(function(data) {

                if(data != undefined && data.error != undefined) {
                    response.value = data.error[0];
                } else {
                    response.value.text = $rootScope.getLocale('Error connection');
                }

                response.error = true;

                callback(response);
            });
        }
    }
}]);

